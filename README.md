# QCAtools R package

## Installation

Installation directly from gitlab is possible, when the package `devtools`
is installed:
```r
library(devtools)
install_git("https://gitlab.com/jirkalewandowski/qcatools.git")
```